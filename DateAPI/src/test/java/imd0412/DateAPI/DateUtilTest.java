package imd0412.DateAPI;

import static org.junit.Assert.assertEquals;

import java.util.Arrays;
import java.util.Collection;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

@RunWith(value = Parameterized.class)
public class DateUtilTest
{
	// Parameterized test attributes
	private int day;
	private int month;
	private int year;
	private String expectedDate;
	
	@Parameters(name = "{index}: nextDate({0}/{1}/{2})")
	public static Collection<Object[]> data() {
		return Arrays.asList(new Object[][] 
		{
			/**
			 * Case Tests
			 * 
			 * Note: they are located in "Casos de Teste" spreadsheet
			 */
			{26, 4, 1992, "27/4/1992"},		// Case-Test-id: 1
			{28, 6, 2017, "29/6/2017"},		// Case-Test-id: 2
			{29, 9, 2017, "30/9/2017"},		// Case-Test-id: 3
			{30, 11, 2016, "1/12/2016"},	// Case-Test-id: 4
			{15, 10, 2015, "16/10/2015"},	// Case-Test-id: 5
			{28, 7, 2014, "29/7/2014"},		// Case-Test-id: 6
			{29, 5, 2015, "30/5/2015"},		// Case-Test-id: 7
			{30, 3, 2013, "31/3/2013"},		// Case-Test-id: 8
			{31, 10, 2012, "1/11/2012"},	// Case-Test-id: 9
			{16, 12, 2013, "17/12/2013"},	// Case-Test-id: 10
			{28, 12, 2016, "29/12/2016"},	// Case-Test-id: 11
			{29, 12, 2009, "30/12/2009"},	// Case-Test-id: 12
			{30, 12, 2004, "31/12/2004"},	// Case-Test-id: 13
			{31, 12, 2010, "1/1/2011"},		// Case-Test-id: 14
			{12, 2, 2009, "13/2/2009"},		// Case-Test-id: 15
			{28, 2, 2016, "29/2/2016"},		// Case-Test-id: 16
			{29, 2, 2012, "1/3/2012"},		// Case-Test-id: 17
			{9, 2, 2015, "10/2/2015"},		// Case-Test-id: 18
			{28, 2, 2017, "1/3/2017"},		// Case-Test-id: 19
			{28, 2, 2000, "29/2/2000"},		// Case-Test-id: 29
			{28, 2, 1900, "1/3/1900"},		// Case-Test-id: 30
		});
	}
	
	/**
	 * Calculates the next date
	 * 
	 * @param day int value representing the date day
	 * @param month int value representing the date month
	 * @param year int value representing the date year
	 * @param expectedDate String representing the next date
	 * @return
	 */
	public DateUtilTest(int day, int month, int year, String expectedDate) {
		// Pass the values to parameterized attributes 
		this.day = day;
		this.month = month;
		this.year = year;
		this.expectedDate = expectedDate;
	}
	
	@Test
	public void checkNextDate() {
		// Date variable
		String nextDate = "";
		
		// Case of test
		nextDate = DateUtil.nextDate(this.day, this.month, this.year);
		
		// Check the result
		assertEquals(this.expectedDate, nextDate);
	}
}