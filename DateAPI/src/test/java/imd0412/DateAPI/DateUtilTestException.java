package imd0412.DateAPI;

import java.util.Arrays;
import java.util.Collection;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

@RunWith(value = Parameterized.class)
public class DateUtilTestException {
	
	@Rule
	public final ExpectedException exception = ExpectedException.none();

	// Parameterized test attributes
	private int day;
	private int month;
	private int year;
	public Class<? extends Exception> expectedException;
	
	@Parameters(name = "{index}: nextDate({0}/{1}/{2})")
	public static Collection<Object[]> data() {
		return Arrays.asList(new Object[][] 
		{
			/**
			 * Case Tests
			 * 
			 * Note: they are located in "Casos de Teste" spreadsheet
			 */
			{32, 1, 2017, IllegalArgumentException.class},		// Case-Test-id: 20
			{0, 2, 2015, IllegalArgumentException.class},		// Case-Test-id: 21
			{10, 13, 2011, IllegalArgumentException.class},		// Case-Test-id: 22
			{15, 0, 2005, IllegalArgumentException.class},		// Case-Test-id: 23
			{5, 9, 1811, IllegalArgumentException.class},		// Case-Test-id: 24
			{6, 12, 2018, IllegalArgumentException.class},		// Case-Test-id: 25
			{29, 2, 2013, IllegalStateException.class},			// Case-Test-id: 26
			{30, 2, 2016, IllegalStateException.class},			// Case-Test-id: 27
		});
	}
	
	/**
	 * Calculates the next date
	 * 
	 * @param day int value representing the date day
	 * @param month int value representing the date month
	 * @param year int value representing the date year
	 * @param expectedDate String representing the next date
	 * @return
	 */
	public DateUtilTestException(int day, int month, int year, Class<? extends Exception> expectedException) {
		// Pass the values to parameterized attributes 
		this.day = day;
		this.month = month;
		this.year = year;
		this.expectedException = expectedException;
	}
	
	/**
	 * Method to test if a defined exception is thrown
	 * 	
	 * @throws DateFormatException
	 * @throws InvalidDataException
	 */
	@Test
	public void testException() throws IllegalArgumentException, IllegalStateException {
	    exception.expect(this.expectedException);
	    
		 // Case of test
		 DateUtil.nextDate(this.day, this.month, this.year);
	}
}