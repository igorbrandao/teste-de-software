package imd0412.DateAPI;

import org.junit.runner.JUnitCore;
import org.junit.runner.Result;
import org.junit.runner.notification.Failure;

public class DateUtilTestSuitRunner {

	public static void main(String[] args) {

		// Run all provided tests
		Result result = JUnitCore.runClasses(DateUtilTestSuite.class);
		
		// If some fail happen, prints the log
		for (Failure fail : result.getFailures()) {
			System.out.println(fail.toString());
		}
		
		// All tests was succeeded
		if (result.wasSuccessful()) {
			System.out.println("All tests finished successfully...");
		}
	}
}