package imd0412.DateAPI;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({
   DateUtilTest.class,
   DateUtilTestException.class
})
public class DateUtilTestSuite {
	// Implementation
}