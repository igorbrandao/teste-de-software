package imd0412.mock;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InOrder;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class SimpleMockExamples 
{
    @Test
    public final void testCreatingMockWithMethod ()
    {
	    List mockList = Mockito.mock(List.class);
	    mockList.add("MyStr");
	    System. out .println(mockList.get(0));
	    System. out .println(mockList.contains("MyStr"));
    }
    
    @Mock
    private List mockList;

    @Test
    public final void testCreatingMockWithAnnotation()
    {
    	mockList.add("MyStr");
    	System.out.println(mockList.get(0));
    	System.out.println(mockList.contains("MyStr"));
    }
    
    @Test(expected=IndexOutOfBoundsException.class)
    public final void testSpecifyingBehavior()
    {
	    // Especificando o comportamento do mock object
	    Mockito.when(mockList.get(0)).thenReturn("MyStr1");
	    Mockito.when(mockList.get(1)).thenReturn("MyStr2");
	    Mockito.when(mockList.get(2)).thenThrow(new IndexOutOfBoundsException());
	    
	    // Exercitando o comportamento do mock object
	    System.out.println(mockList.get(0)); // prints MyStr1
	    System.out.println(mockList.get(1)); // prints MyStr2
	    System.out.println(mockList.get(9)); // prints null
	    System.out.println(mockList.get(2)); // throws exception
    }
    
    @Test
    public final void testSpecifyingBehaviorWithArgumentMatchers()
    {
    	Mockito.when(mockList.get(Mockito.anyInt())).thenReturn("MyStrMatcher");
	    System.out.println(mockList.get(0));
	    System.out.println(mockList.get(1));
	    System.out.println(mockList.get(9));
	    System.out.println(mockList.get(2));
    }
    
    @Test
    public final void testSpecifyingBehaviorWithSuccessiveInvocations()
    {
	    Mockito.when(mockList.get(Mockito.anyInt())).thenReturn("S1", "S2", "S3");
	    System.out.println(mockList.get(0));
	    System.out.println(mockList.get(1));
	    System.out.println(mockList.get(9));
	    System.out.println(mockList.get(2));
	    System.out.println(mockList.get(2));
    }
    
    @Test(expected = UnsupportedOperationException.class)
    public final void testSpecifyingBehaviorOfVoidMethods()
    {
	    Mockito.doThrow (new UnsupportedOperationException()).when(mockList).clear();
	    mockList.clear();
    }
    
    @Test
    public final void testVerifyingBehavior()
    {
	    // Exercitando o mock object
	    mockList.add("A");
	    mockList.clear();
	    // Verificando se determinadas invocações foram feitas durante o exercício
	    Mockito.verify(mockList).clear();
	    Mockito.verify(mockList).add("A");
    }
    
    @Test
    public final void testVerifyingNumberOfInvocations()
    {
	    // Exercitando o mock object
	    mockList.add("once");
	    mockList.add("twice");
	    mockList.add("twice");
	    
	    // As duas verificações são equivalentes: times(1) é usada por default
	    Mockito.verify(mockList).add("once");
	    Mockito.verify(mockList, Mockito.times(1)).add("once");
	    
	    // Verifica o número exato de invocações
	    Mockito.verify(mockList, Mockito.times(2)).add("twice");
	    
	    // Verifica que uma invocação nunca ocorreu.
	    // never() é um ‘apelido’ para times(0)
	    Mockito.verify(mockList, Mockito.never()).add("never happened");
	    
	    // Verificando que ao menos algumas invocações ocorreram - atLeast()
	    // e que no máximo algumas invocações ocorreram - atMost()
	    Mockito.verify(mockList, Mockito.atLeastOnce()).add("twice");
	    Mockito.verify(mockList, Mockito.atLeast(2)).add("twice");
	    Mockito.verify(mockList, Mockito.atMost(5)).add("twice");
    }
    
    @Test
    public final void testVerifyingOrderOfInvocations()
    {
	    // Exercita o mock object
	    mockList.add("A");
	    mockList.add("B");
	    mockList.clear();
	    
	    // Cria um 'verificador' de ordem
	    InOrder inOrder = Mockito.inOrder(mockList);
	    
	    // Verificações a seguir checam se foi obedecida esta ordem de invocações
	    inOrder.verify(mockList).add("A");
	    inOrder.verify(mockList).add("B");
	    inOrder.verify(mockList).clear();
    }
}