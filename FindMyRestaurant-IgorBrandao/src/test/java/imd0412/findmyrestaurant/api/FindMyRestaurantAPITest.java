package imd0412.findmyrestaurant.api;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import imd0412.findmyrestaurant.domain.Restaurant;

@RunWith(value = Parameterized.class)
public class FindMyRestaurantAPITest
{
	// Parameterized test attributes
	private String address;
	private String restaurantName;
	private String specialty;
	private List<Restaurant> expectedRestaurantList;
	
	// Restaurant static test list
	private static List<Restaurant> churrascaria = Arrays.asList
			(new Restaurant("Sal e Brasa", "", "Churrascaria"),
			 new Restaurant("Churrascaria do Arnaldo", "", "Churrascaria"),
			 new Restaurant("Fogo e Chama", "", "Churrascaria"));
	
	private static List<Restaurant> fast_food = Arrays.asList
			(new Restaurant("Habibs", "", "Fast-food"),
			 new Restaurant("Burguer King", "", "Fast-food"),
			 new Restaurant("Giraffas", "", "Fast-food"),
			 new Restaurant("Subway", "", "Fast-food"),
			 new Restaurant("Pitts Burguer", "", "Fast-food"),
			 new Restaurant("McDonalds", "", "Fast-food"));
	
	private static List<Restaurant> japa = Arrays.asList
			(new Restaurant("Irachai", "", "Comida Japonesa"),
			 new Restaurant("Sushi Delli", "", "Comida Japonesa"));
	
	private static List<Restaurant> self_service = Arrays.asList
			(new Restaurant("Mangai", "", "Self-service"),
			 new Restaurant("Almirante", "", "Self-service"),
			 new Restaurant("Pinga Fogo", "", "Self-service"),
			 new Restaurant("Padaria Bonfim", "", "Self-service"));
	
	private static List<Restaurant> italiano = Arrays.asList
			(new Restaurant("Spolleto", "", "Comida Italiana"));
	
	@Parameters(name = "{index}: findNearRestaurantsBySpeciality({0}/{1}/{2})")
	public static Collection<Object[]> data() {
		return Arrays.asList(new Object[][] 
		{
			/**
			 * Case Tests
			 */
			{"Av. Engenheiro Roberto Freire, 1070", "Sal e Brasa", "Churrascaria", churrascaria},
			{"Av Senador Salgado Filho 2234", "Burguer King", "Fast-food", fast_food},
			{"Av. Bernardo Vieira, 3775", "Spoleto", "Comida Italiana", italiano},
			{"Av Senador Salgado Filho 2234", "Irachai", "Comida Japonesa", japa},
			{"Av. Amintas Barros, 3300", "Mangai", "Self-service", self_service},
		});
	}

	/**
	 * Class constructor
	 * Pass the values to parameterized attributes
	 * 
	 * @param String address_ => the informations related to address
	 * @param String restaurantName_ => the restaurant name itself
	 * @param String specialty_ => the restaurant specialty description
	 * @param List<Restaurant> expectedRestaurantList_ => the expected list of restaurants
	 * @return
	 */
	public FindMyRestaurantAPITest(String address_, String restaurantName_, String specialty_, 
			List<Restaurant> expectedRestaurantList_) {
		// Pass the values to parameterized attributes 
		this.address = address_;
		this.restaurantName = restaurantName_;
		this.specialty = specialty_;
		this.expectedRestaurantList = expectedRestaurantList_;
	}

	@Mock
	private FindMyRestaurantAPI api;
	   
    @Before
	public void init(){
    	MockitoAnnotations.initMocks(this);
    	// api = new FindMyRestaurantAPI();
    }
	   
    @Test
    public void findNearRestaurantsBySpecialityTest() {
    	// Verify each test instance return
    	Mockito.when(api.findNearRestaurantsBySpeciality(this.address, this.specialty)).thenReturn(this.expectedRestaurantList);
    	
    	// Check if the test was performed
    	Mockito.verify(api, Mockito.atLeastOnce()).findNearRestaurantsBySpeciality(this.address, this.specialty);
    }
	   
    @Test(expected=Exception.class)
    public void findNearRestaurantsBySpecialityNullTest() {
    	// Verify if each test instance return an exception
    	Mockito.when(api.findNearRestaurantsBySpeciality(this.address, this.specialty)).thenThrow(new Exception());
    }
}